<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<div <?php post_class( 'post' ); ?>>
		<?php crb_the_title( '<h2 class="pagetitle">', '</h2>' ); ?>

		<div class="entry">
			<?php
			the_content( __( '<p class="serif">' . __( 'Read the rest of this page &raquo;', 'crb' ) . '</p>' ) );
			edit_post_link( __( 'Edit this entry.', 'crb' ), '<p>', '</p>' );
			?>
		</div>
	</div>
<?php endwhile; ?>

<?php get_footer(); ?>
