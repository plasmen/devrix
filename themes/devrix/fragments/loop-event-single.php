<?php
while ( have_posts() ) : the_post();
	$event = Dvx_Event::make();
	?>
	<article <?php post_class( 'article article-single' ) ?>>
		<header class="article-head">
			<div class="article-head-left">
				<h2 class="article-title">
					<?php the_title(); ?>
				</h2><!-- /.article-title -->

				<?php get_template_part( 'fragments/event/meta' ); ?>
			</div><!-- /.article-head-left -->

			<div class="article-head-right">
				<?php crb_render_fragment( 'fragments/event/button-calendar', compact( 'event' ) ); ?>
			</div><!-- /.article-head-right -->
		</header><!-- /.article-head -->

		<div class="article-body">
			<div class="article-entry">
				<?php crb_render_fragment( 'fragments/event/location', compact( 'event' ) ); ?>

				<?php the_content(); ?>

				<?php crb_render_fragment( 'fragments/event/button-external', compact( 'event' ) ); ?>
			</div><!-- /.article-entry -->

		</div><!-- /.article-body -->
	</article><!-- /.article -->
<?php endwhile; ?>
