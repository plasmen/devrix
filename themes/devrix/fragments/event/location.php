<?php
$location = $event->get_location();
if ( ! empty( $location ) ): ?>
	<div class="event-location">
		<div class="event-map">
			<div
				class="google-map google-map-<?php the_id(); ?>"
				id="google-map-<?php the_id(); ?>"
				data-lat="<?php echo esc_attr( $location['lat'] ); ?>"
				data-lng="<?php echo esc_attr( $location['lng'] ); ?>"
			></div><!-- /#google-map-ID.google-map google-map-ID -->
		</div><!-- /.event-map -->
	</div><!-- /.event-location -->
<?php endif; ?>
