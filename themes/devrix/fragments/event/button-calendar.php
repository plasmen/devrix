<?php
$calendar_link = $event->get_calendar_link();
if ( ! empty( $calendar_link ) ): ?>
	<a href="<?php echo esc_url( $calendar_link ); ?>" class="btn btn-primary btn-sm" target="_blank">
		<?php _e( 'Add to Google Calendar', 'crb' ); ?>
	</a>
<?php endif; ?>
