<?php
/**
 * Displays the post date/time, author, tags, categories and comments link.
 *
 * Should be called only within The Loop.
 *
 * It will be displayed only for post type "post".
 */

if ( empty( $post ) || get_post_type() !== 'dvx_event' ) {
	return;
}

$event = Dvx_Event::make();

?>

<div class="article__meta">
	<p>
		<?php echo apply_filters( 'the_title', $event->get_date_text() ); ?>
	</p>
</div><!-- /.article__meta -->
