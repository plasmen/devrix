<?php
$external_link = $event->get_external_link();
if ( ! empty( $external_link ) ): ?>
	<a href="<?php echo esc_url( $external_link ); ?>" class="btn btn-info btn-sm" target="_blank">
		<?php _e( "Visit Event's Page", 'crb' ); ?>
	</a>
<?php endif; ?>
