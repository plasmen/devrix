<?php

define( 'DVX_THEME_DIR', dirname( __FILE__ ) . DIRECTORY_SEPARATOR );

# Enqueue JS and CSS assets on the front-end
add_action( 'wp_enqueue_scripts', 'dvx_wp_enqueue_scripts' );
function dvx_wp_enqueue_scripts() {
	$template_dir = get_template_directory_uri();

	# Enqueue jQuery
	wp_enqueue_script( 'jquery' );

	# Enqueue Custom JS files
	$api_key = apply_filters( 'carbon_fields_map_field_api_key', false );
	$url = '//maps.googleapis.com/maps/api/js?' . ( $api_key ? 'key=' . $api_key : '' );
	wp_enqueue_script( 'google-maps', $url, array(), null );

	crb_enqueue_script( 'theme-maps', $template_dir . '/js/google-map.js', array( 'jquery' ) );
	// crb_enqueue_script( 'theme-functions', $template_dir . '/js/functions.js', array( 'jquery' ) );

	# Enqueue Custom CSS files
	wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' );
	crb_enqueue_style( 'theme-styles', $template_dir . '/style.css' );

	# Enqueue Comments JS file
	if ( is_singular() ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

# Attach Custom Post Types and Custom Taxonomies
add_action( 'init', 'dvx_attach_post_types_and_taxonomies', 0 );
function dvx_attach_post_types_and_taxonomies() {
	# Attach Custom Post Types
	include_once( DVX_THEME_DIR . 'options/post-types.php' );

	# Attach Custom Taxonomies
	include_once( DVX_THEME_DIR . 'options/taxonomies.php' );
}

add_action( 'after_setup_theme', 'crb_setup_theme' );

# To override theme setup process in a child theme, add your own crb_setup_theme() to your child theme's
# functions.php file.
if ( ! function_exists( 'crb_setup_theme' ) ) {
	function crb_setup_theme() {
		# Make this theme available for translation.
		load_theme_textdomain( 'crb', get_template_directory() . '/languages' );

		# Autoload dependencies
		$autoload_dir = DVX_THEME_DIR . 'vendor/autoload.php';
		if ( ! is_readable( $autoload_dir ) ) {
			wp_die( __( 'Please, run <code>composer install</code> to download and install the theme dependencies.', 'crb' ) );
		}
		include_once( $autoload_dir );
		\Carbon_Fields\Carbon_Fields::boot();

		# Additional libraries and includes
		include_once( DVX_THEME_DIR . 'includes/title.php' );
		include_once( DVX_THEME_DIR . 'includes/Dvx_Event.php' );

		# Theme supports
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'gallery' ) );

		# Manually select Post Formats to be supported - http://codex.wordpress.org/Post_Formats
		// add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );

		# Register Theme Menu Locations
		/*
		register_nav_menus(array(
			'main-menu' => __( 'Main Menu', 'crb' ),
		));
		*/

		# Add Actions
		add_action( 'carbon_fields_register_fields', 'dvx_attach_theme_options' );

		# Add Filters
		add_filter( 'excerpt_more', 'dvx_excerpt_more' );
		add_filter( 'excerpt_length', 'dvx_excerpt_length', 999 );
		add_filter( 'carbon_fields_map_field_api_key', 'dvx_get_google_maps_api_key' );
	}
}

function dvx_attach_theme_options() {
	# Attach fields
	include_once( DVX_THEME_DIR . 'options/theme-options.php' );
	include_once( DVX_THEME_DIR . 'options/post-meta.php' );
}

/**
 * Returns the Google Maps API Key set in Theme Options.
 *
 * @return string
 */
function dvx_get_google_maps_api_key() {
	return carbon_get_theme_option( 'crb_google_maps_api_key' );
}

function dvx_excerpt_more() {
	return '...';
}

function dvx_excerpt_length() {
	return 55;
}

add_action( 'pre_get_posts', 'dvx_pre_get_posts' );
function dvx_pre_get_posts( $query ) {
	if ( $query->is_main_query() && $query->is_post_type_archive( 'dvx_event' ) ) {
		$query->set( 'order', 'ASC' );
		$query->set( 'orderby', 'meta_value' );
		$query->set( 'meta_key', '_dvx_event_date_start' );
		$query->set( 'meta_type', 'DATE' );

		// Don't break admin Queries
		if ( ! is_admin() ) {
			$query->set( 'meta_query', array(
				/**
				Sample:

				NOW is 21st

				Event 1 - 1st-2nd - started and ended before
				Event 2 - 11st-29th - started before, not yet ended
				Event 3 - 25th-26th - not yet starter or ended

				**/
				'relation' => 'OR',
				array(
					'key' => '_dvx_event_date_start',
					'value' => date( 'Y-m-d' ),
					'type' => 'DATE',
					'compare' => '>=',
				),
				array(
					'key' => '_dvx_event_date_end',
					'value' => date( 'Y-m-d' ),
					'type' => 'DATE',
					'compare' => '>=',
				),
			) );
		}
	}
}

/**
 * Recursive function for retrieving a value by a given variable route in a specific array or object
 * Return false if the variable isn't available
 *
 * Examples:
 * $variable_route=ID, $arr_obj=new WP_POST, return => $arr_obj->ID
 * $variable_route=post|post_name, $arr_obj=(object), return => $arr_obj->post->post_name
 *
 * @param  string $variable_route
 * @param  mixed $arr_obj
 * @return a value specified by a variable route from a given array or object
 */
function crb_get_var( $variable_route, $arr_obj, $default_value = '' ) {
	if ( ! $arr_obj ) {
		return $default_value;
	}

	$route_parts = explode( '|', $variable_route );
	$var_name = trim( $route_parts[0] );

	$result = '';

	// get the value
	if ( is_object( $arr_obj ) && ! empty( $arr_obj->$var_name ) ) {
		$result = $arr_obj->$var_name;
	} else if ( is_array( $arr_obj ) && ! empty( $arr_obj[$var_name] ) ) {
		$result = $arr_obj[$var_name];
	} else {
		return $default_value;
	}

	if ( count( $route_parts ) > 1 ) {
		unset( $route_parts[0] );
		return crb_get_var( implode( '|', $route_parts ), $result, $default_value );
	} else {
		return $result;
	}
}

// Using Geocoding convert coordinates into an address
function dvx_coordinates_to_address( $lat, $lng ) {
	$cache_key = 'dvx_address_' . md5( $lat . $lng );
	$cache = get_transient( $cache_key );
	if ( $cache !== false ) {
		return $cache;
	}

	$base_url = 'https://maps.googleapis.com/maps/api/geocode/json';
	$api_key = carbon_get_theme_option( 'crb_google_maps_api_key_backend' );

	$url = add_query_arg( array(
		'key' => $api_key,
		'latlng' => implode( ',', array( $lat, $lng ) ),
	), $base_url );

	$response = wp_remote_get( $url );
	$body = $response['body'];
	$geocode = json_decode( $body );

	$address = crb_get_var( 'results|0|formatted_address', $geocode );

	set_transient( $cache_key, $address, 3 * MONTH_IN_SECONDS );

	return $address;
}
