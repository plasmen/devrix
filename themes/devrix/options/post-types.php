<?php

register_post_type( 'dvx_event', array(
	'labels' => array(
		'name' => __( 'Events', 'crb' ),
		'singular_name' => __( 'Event', 'crb' ),
		'add_new' => __( 'Add New', 'crb' ),
		'add_new_item' => __( 'Add new Event', 'crb' ),
		'view_item' => __( 'View Event', 'crb' ),
		'edit_item' => __( 'Edit Event', 'crb' ),
		'new_item' => __( 'New Event', 'crb' ),
		'view_item' => __( 'View Event', 'crb' ),
		'search_items' => __( 'Search Events', 'crb' ),
		'not_found' =>  __( 'No events found', 'crb' ),
		'not_found_in_trash' => __( 'No events found in trash', 'crb' ),
	),
	'public' => true,
	'exclude_from_search' => false,
	'show_ui' => true,
	'has_archive' => true,
	'capability_type' => 'page',
	'hierarchical' => false,
	'rewrite' => array(
		'slug' => 'event',
		'with_front' => false,
	),
	'query_var' => true,
	'menu_icon' => 'dashicons-calendar',
	'supports' => array( 'title', 'editor', 'page-attributes', 'thumbnail' ),
) );
