<?php

use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'post_meta', __( 'Event Settings', 'crb' ) )
	->show_on_post_type( 'dvx_event' )
	->add_fields( array(
		Field::make( 'date', 'dvx_event_date_start', __( 'Start Date', 'crb' ) )
			->set_required( true )
			->set_width( 50 ),
		Field::make( 'date', 'dvx_event_date_end', __( 'End Date', 'crb' ) )
			->set_required( true )
			->set_width( 50 ),
		Field::make( 'map', 'dvx_event_location', __( 'Location', 'crb' ) ),
		Field::make( 'text', 'dvx_event_external_link', __( 'External URL', 'crb' ) ),
	) );
