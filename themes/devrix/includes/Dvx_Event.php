<?php

/**
 * Handles all Event data retriaval
 */
class Dvx_Event {
	private static $instances = array();

	private function __construct( $post_id ) {
		$this->id = $post_id;
		$this->post = get_post( $post_id );
	}

	public static function make( $post_id = null ) {
		if ( empty( $post_id ) ) {
			$post_id = get_the_id();
		}

		if ( ! isset( self::$instances[$post_id] ) ) {
			self::$instances[$post_id] = new Dvx_Event( $post_id );
		}

		return self::$instances[$post_id];
	}

	public function get_date_text() {
		if ( isset( $this->meta['date_text'] ) ) {
			return $this->meta['date_text'];
		}

		$start = $this->get_date_start();
		$end = $this->get_date_end();

		$start_timestamp = strtotime( $start );
		$end_timestamp = strtotime( $end );

		$start_year = date( 'Y', $start_timestamp );
		$end_year = date( 'Y', $end_timestamp );
		$start_month = date( 'm', $start_timestamp );
		$end_month = date( 'm', $end_timestamp );
		$start_day = date( 'd', $start_timestamp );
		$end_day = date( 'd', $end_timestamp );

		$is_same_year = $start_year === $end_year;
		$is_same_month = $start_month === $end_month;
		$is_same_day = $start_day === $end_day;

		$date_text = '';
		// F jS, Y
		// January 20th, 2018
		if ( $start == $end ) {
			$date_text = date( 'F jS, Y', $start_timestamp );

		// January 20th - 21st, 2018
		} elseif ( $is_same_year && $is_same_month && ! $is_same_day ) {
			$date_text = sprintf(
				'%s %s - %s, %s',
				date( 'F', $start_timestamp ),
				date( 'jS', $start_timestamp ),
				date( 'jS', $end_timestamp ),
				date( 'Y', $start_timestamp )
			);

		// January 20th - February 21st, 2018
		} elseif ( $is_same_year && ! $is_same_month ) {
			$date_text = sprintf(
				'%s - %s, %s',
				date( 'F jS', $start_timestamp ),
				date( 'F jS', $end_timestamp ),
				date( 'Y', $start_timestamp )
			);

		// January 20th, 2018 - January 20th, 2019
		} elseif ( ! $is_same_year ) {
			$date_text = sprintf(
				'%s - %s',
				date( 'F jS, Y', $start_timestamp ),
				date( 'F jS, Y', $end_timestamp )
			);
		}

		$this->meta['date_text'] = $date_text;

		return $this->meta['date_text'];
	}

	public function get_location() {
		if ( isset( $this->meta['location'] ) ) {
			return $this->meta['location'];
		}

		$location = carbon_get_post_meta( $this->id, 'dvx_event_location' );

		$this->meta['location'] = $location;

		return $this->meta['location'];
	}

	public function get_address() {
		if ( isset( $this->meta['address'] ) ) {
			return $this->meta['address'];
		}

		$location = $this->get_location();
		if ( empty( $location ) ) {
			$this->meta['address'] = '';
			return $this->meta['address'];
		}

		$this->meta['address'] = dvx_coordinates_to_address( $location['lat'], $location['lng'] );

		return $this->meta['address'];
	}

	public function get_external_link() {
		if ( isset( $this->meta['external_link'] ) ) {
			return $this->meta['external_link'];
		}

		$external_link = carbon_get_post_meta( $this->id, 'dvx_event_external_link' );

		$this->meta['external_link'] = $external_link;

		return $this->meta['external_link'];
	}

	public function get_date_start() {
		if ( isset( $this->meta['date_start'] ) ) {
			return $this->meta['date_start'];
		}

		$date_start = carbon_get_post_meta( $this->id, 'dvx_event_date_start' );

		$this->meta['date_start'] = $date_start;

		return $this->meta['date_start'];
	}

	public function get_date_end() {
		if ( isset( $this->meta['date_end'] ) ) {
			return $this->meta['date_end'];
		}

		$date_end = carbon_get_post_meta( $this->id, 'dvx_event_date_end' );

		$this->meta['date_end'] = $date_end;

		return $this->meta['date_end'];
	}

	public function get_excerpt() {
		if ( isset( $this->meta['excerpt'] ) ) {
			return $this->meta['excerpt'];
		}

		setup_postdata( $this->post );
		$excerpt = get_the_excerpt();
		wp_reset_postdata();

		$this->meta['excerpt'] = $excerpt;

		return $this->meta['excerpt'];
	}

	/**
	 * Unofficial URL for creating Google Event
	 * Documentation has existed long time ago
	 * http://useroffline.blogspot.com/2009/06/making-google-calendar-link.html
	 * https://web.archive.org/web/20120313011336/http://www.google.com/googlecalendar/event_publisher_guide.html
	 * https://calendar.google.com/calendar/r/eventedit
	 *   text     => Your+Event+Name
	 *   dates    => "20140127T224000Z/20140320T221500Z" OR "20140127/20140320"
	 *   details  => For+details,+link+here:+http://www.example.com
	 *   location => Waldorf+Astoria,+301+Park+Ave+,+New+York,+NY+10022
	 *   sf       => true
	 *   output   => xml
	 */
	public function get_calendar_link() {
		if ( isset( $this->meta['calendar_link'] ) ) {
			return $this->meta['calendar_link'];
		}

		$base_url = 'https://calendar.google.com/calendar/r/eventedit';

		$start = $this->get_date_start();
		$end = $this->get_date_end();

		$start_timestamp = strtotime( $start );
		$end_timestamp = strtotime( $end );

		$location = '';
		$location_meta = $this->get_location();
		if ( ! empty( $location_meta ) ) {
			$location = sprintf( '%s,%s', $location_meta['lat'], $location_meta['lng'] );
		}

		$calendar_link = add_query_arg( array(
			'text'     => urlencode( $this->post->post_title ),
			'dates'    => sprintf( '%s/%s', date( 'Ymd', $start_timestamp ), date( 'Ymd', $end_timestamp ) ),
			'details'  => urlencode( $this->get_excerpt() ),
			'location' => urlencode( $this->get_address() ),
			'sf'       => 'true',
			'output'   => 'xml',
		), $base_url );

		$this->meta['calendar_link'] = $calendar_link;

		return $this->meta['calendar_link'];
	}
}
