<?php
get_header();

crb_the_title( '<h2 class="pagetitle">', '</h2>' );
get_template_part( 'fragments/loop', 'event-single' );

get_footer();
