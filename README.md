## Assignment

Create a simple event listing

A new custom post type for Events

The custom post type needs to have a few custom fields:

 * event date (date picker)
 * event location
 * URL for the event

We need an archive page which will list all events, ordered by event date. Each event should have a title, location (you can add a Google map integration), event date, a link to the external source/site.
We need to have a simple "Add to Google Calendar" button, which will add the event to your google calendar.

We are using Git, so creating a GitHub/Bitbucket repository with the source code would be a good start.

The project should follow the latest WordPress Coding standards

## Preview

http://devrix.plasmen.info/event/
